const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Business
let Products = new Schema({
    p_id: {
        type: Number
    },
    name: {
        type: String
    },
    price: {
        type: Number
    },
    thumb: {
        type: String
    },
    category: {
        type: String
    }
}, {
    collection: 'products'
});

module.exports = mongoose.model('Products', Products);