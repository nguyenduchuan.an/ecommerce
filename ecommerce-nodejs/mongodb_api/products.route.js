const express = require('express');
const productRoutes = express.Router();

// Require Business model in our routes module
let product = require('./products.modal');

// Defined store route
productRoutes.route('/add').post(function (req, res) {
    let product = new product(req.body);
    product.save()
        .then(product => {
            res.status(200).json({'product': 'product in added successfully'});
        })
        .catch(err => {
            res.status(400).send("unable to save to database");
        });
});

// Defined get data(index or listing) route
productRoutes.route('/').get(function (req, res) {
    product.find(function(err, products){
        if(err){
            console.log(err);
        }
        else {
            res.json(products);
        }
    });
});

// Defined edit route
productRoutes.route('/edit/:id').get(function (req, res) {
    let id = req.params.id;
    product.findById(id, function (err, business){
        res.json(business);
    });
});

//  Defined update route
productRoutes.route('/update/:id').post(function (req, res) {
    product.findById(req.params.id, function(err, product) {
        if (!product)
            res.status(404).send("data is not found");
        else {
            console.log(product);
            product.p_id = req.body.p_id;
            product.name = req.body.name;
            product.price = req.body.price;
            product.thumb = req.body.thumb;
            product.category = req.body.category;

            product.save().then(business => {
                res.json('Update complete');
            })
                .catch(err => {
                    res.status(400).send("unable to update the database");
                });
        }
    });
});

// Defined delete | remove | destroy route
productRoutes.route('/delete/:id').get(function (req, res) {
    product.findByIdAndRemove({_id: req.params.id}, function(err, product){
        if(err) res.json(err);
        else res.json('Successfully removed');
    });
});

module.exports = productRoutes;